
Generated on 2019-10-28 06:33:32.162259

Assignments Submitted: [![coverage report](https://gitlab.com/gitcourses/iiitmk-ai-2019/badges/Master/coverage.svg)](https://gitlab.com/gitcourses/iiitmk-ai-2019/commits/Master)

| name                  | HackerRank | Assignment | email                        | hackerrank link                            |
| --------------------- | ---------- | ---------- | ---------------------------- | -------------------------------------------|
| AbittaVR              | 20         | 0          | abitta.da3@iiitmk.ac.in      | https://www.hackerrank.com/abitta_da3      |
| Ajil                  | 20         | 0          | ajil.da3@iiitmk.ac.in        | https://www.hackerrank.com/mails_47        |
| Akhil-K-K             | 0          | 1          | akhil.mi3@iiitmk.ac.in       | https://www.hackerrank.com/akhilkishore    |
| akhil-siby            | 0          | 0          | akhil.da3@iiitmk.ac.in       | None                                       |
| Alida-Baby            | 0          | 0          | alda.mi3@iiitmk.ac.in        | None                                       |
| Anandha-Krishnan-H    | 20         | 1          | anandha.mi3@iiitmk.ac.in     | https://www.hackerrank.com/AnandhaKrishnanH|
| anandhu-bhaskar       | 0          | 0          | anandhu.da3@iiitmk.ac.in     | None                                       |
| anitta-augustine      | 20         | 0          | anitta.da3@iiitmk.ac.in      | https://www.hackerrank.com/anittaug        |
| Anju-Vinod            | 0          | 0          | anju.da3@iiitmk.ac.in        | None                                       |
| ann-mary              | 20         | 0          | ann.mi3@iiitmk.ac.in         | https://www.hackerrank.com/ann_mi3         |
| anu-elizabath-shibu   | 20         | 0          | anu.da3@iiitmk.ac.in         | https://www.hackerrank.com/anu_da3         |
| anushka-srivastava    | 20         | 1          | anushka.da3@iiitmk.ac.in     | https://www.hackerrank.com/anushka_da3     |
| arjoonn               | 16         | 0          | arjoonn.msccsc5@iiitmk.ac.in | https://www.hackerrank.com/arjoonn         |
| Arvind                | 0          | 0          | arvind.mi3@iiitmk.ac.in      | None                                       |
| Ashwiniseelan         | 20         | 0          | ashwini.da3@iiitmk.ac.in     | https://www.hackerrank.com/ashwini_da3     |
| brittosabu            | 20         | 1          | britto.da3@iiitmk.ac.in      | https://www.hackerrank.com/brittosabu07    |
| chinju-murali         | 20         | 1          | chinju.mi3@iiitmk.ac.in      | https://www.hackerrank.com/chinjumurali    |
| Dhanesh               | 0          | 0          | dhanesh.mi3@iiitmk.ac.in     | None                                       |
| Giridhar_K            | 20         | 0          | giridhar.da3@iiitmk.ac.in    | https://www.hackerrank.com/giridhar_da3    |
| gokul-p               | 0          | 0          | gokul.da3@iiitmk.ac.in       | None                                       |
| hashim-abdulla        | 0          | 0          | hashim.mi3@iiitmk.ac.in      | None                                       |
| Hima-Santhosh         | 20         | 0          | hima.da3@iiitmk.ac.in        | https://www.hackerrank.com/hima_da3        |
| jose-vincent          | 20         | 1          | jose.da3@iiitmk.ac.in        | https://www.hackerrank.com/jose_vincent    |
| Lipsa                 | 20         | 1          | lipsa.da3@iiitmk.ac.in       | https://www.hackerrank.com/lipsajohny      |
| malavika-james        | 20         | 0          | malavika.da3@iiitmk.ac.in    | https://www.hackerrank.com/malavika_da3    |
| megha-ghosh           | 8          | 0          | megha.mi3@iiitmk.ac.in       | https://www.hackerrank.com/megha_f1        |
| meghana-muraleedharan | 0          | 0          | meghana.da3@iiitmk.ac.in     | None                                       |
| mobin-m               | 18         | 1          | mobin.mi3@iiitmk.ac.in       | https://www.hackerrank.com/theDEMYSTIFier  |
| nasim-sulaiman        | 20         | 1          | nasim.mi3@iiitmk.ac.in       | https://www.hackerrank.com/nasim_mi3       |
| navya-jose            | 20         | 0          | navya.mi3@iiitmk.ac.in       | https://www.hackerrank.com/navyajose95     |
| nithin-g              | 20         | 1          | nithin.da3@iiitmk.ac.in      | https://www.hackerrank.com/nithin_da3      |
| nitish                | 20         | 1          | nitish.mi3@iiitmk.ac.in      | https://www.hackerrank.com/niteshmichael   |
| OliviTJ               | 0          | 0          | None                         | None                                       |
| pallavi-pannu         | 20         | 1          | pallavi.da3@iiitmk.ac.in     | https://www.hackerrank.com/pallavi_da3     |
| prabhatika            | 20         | 1          | prabhatika.mi3@iiitmk.ac.in  | https://www.hackerrank.com/prabhatika_vij  |
| PRAVEEN-PRATIK        | 0          | 0          | praveen.da3@iiitmk.ac.in     | None                                       |
| princ3                | 18         | 1          | prince.mi3@iiitmk.ac.in      | https://www.hackerrank.com/logontoprince   |
| sagnik-mukherjee      | 20         | 1          | sagnik.mi3@iiitmk.ac.in      | https://www.hackerrank.com/sagnik_mi3      |
| sandeep_ma            | 20         | 1          | sandeep.da3@iiitmk.ac.in     | https://www.hackerrank.com/sandeep_da3     |
| Sanjumariam           | 20         | 1          | sanju.mi3@iiitmk.ac.in       | https://www.hackerrank.com/sanju_mi3       |
| Shravan               | 0          | 0          | shravan.da3@iiitmk.ac.in     | None                                       |
| sidharth-manmadhan    | 0          | 0          | sidharth.mi3@iiitmk.ac.in    | None                                       |
| Sreehari-P-V          | 6          | 1          | sreehari.mras@gmail.com      | https://www.hackerrank.com/sreehari_mi3    |
| sreyac                | 0          | 0          | None                         | None                                       |
| sukesh_s              | 16         | 1          | sukesh.mi3@iiitmk.ac.in      | https://www.hackerrank.com/sukesh_mi3      |
| Tathagata-Ghosh       | 20         | 0          | tathagata.da3@iiitmk.ac.in   | https://www.hackerrank.com/tathagata_da3   |
| ummarshaik            | 20         | 0          | ummar.da3@iiitmk.ac.in       | https://www.hackerrank.com/ummar_da3       |
| VaibhawKumar          | 20         | 0          | vaibhaw.da3@iiitmk.ac.in     | https://www.hackerrank.com/vaibhaw_da3     |
| vinu-abraham          | 20         | 1          | vinuabraham.mi3@iiitmk.ac.in | https://www.hackerrank.com/vinuabraham_mi3 |
| vinu-alex             | 0          | 0          | vinu.mi3@iiitmk.ac.in        | None                                       |
